import utils.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;

public class ClientThread extends Thread {
    private Socket socket;
    private BufferedReader br;
    private BufferedWriter bw;
    private static final String CONNECT = "CONNECT";

    public ClientThread(Socket socket) {
        this.socket = socket;
    }


    @Override
    public void run() {
        try {
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
//            socket.getOutputStream().write(("HTTP/1.0 200 Connection established\r\n" + "Proxy-Agent: ProxyServer/1.0\r\n" + "\r\n").getBytes());
//            socket.getOutputStream().flush();

            String request = br.readLine();

            String requestType = request.substring(0, request.indexOf(' '));

            String domain = request.substring(request.indexOf(' ') + 1);


            domain = domain.substring(0, domain.indexOf(' '));

            if (FileUtils.isBlockedDomain(domain.substring(0, domain.lastIndexOf(".com") + 4))) {
                System.out.println("BLOQUEADO");
                socket.getOutputStream().write(("HTTP/1.0 403 Access Forbidden \n" + "User-Agent: ProxyServer/1.0\n" + "\r\n").getBytes());
                socket.getOutputStream().write(("<html><head></head><body>BLOQUEADO :/</body></html>").getBytes());
                socket.getOutputStream().flush();
                return;
            }
            if (!domain.substring(0, 4).equals("http")) {
                String temp = "http://";
                domain = temp + domain;
            }

            if (requestType.equals(CONNECT)) {

                https(domain);
            } else {
                sendNonCachedToClient(domain);
            }

            System.out.println(domain);

        } catch (IOException e) {

        }

    }

    private void https(String domain) {

        String url = domain.substring(7);
        String pieces[] = url.split(":");
        url = pieces[0];
        int port = Integer.valueOf(pieces[1]);

        try {

            for (int i = 0; i < 4; i++) {
                br.readLine();
            }


            InetAddress address = InetAddress.getByName(url);

            Socket server = new Socket(address, port);


            bw.write("HTTP/1.0 200 Connection established\r\n" + "Proxy-Agent: ProxyServer/1.0\r\n" + "\r\n");
            bw.flush();


            DomainClient domainClient = new DomainClient(socket.getInputStream(), server.getOutputStream());

            domainClient.start();

            try {
                sendServerResponse(server.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (server != null) {
                server.close();
            }

            server.getInputStream().close();
            server.getOutputStream().close();

            if (bw != null) {
                bw.close();
            }


        } catch (SocketTimeoutException e) {
            String line = "HTTP/1.0 504 Timeout Occured after 10s\n" +
                    "User-Agent: ProxyServer/1.0\n" +
                    "\r\n";
            try {
                bw.write(line);
                bw.flush();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } catch (Exception e) {

        }
    }

    private void
    sendServerResponse(InputStream serverInput) throws IOException {

        byte[] buffer = new byte[4096];
        int read;
        do {
            read = serverInput.read(buffer);
            if (read > 0) {
                socket.getOutputStream().write(buffer, 0, read);
                if (serverInput.available() < 1) {
                    socket.getOutputStream().flush();
                }
            }
        } while (read >= 0);
    }

    private void sendNonCachedToClient(String urlString) {

        try {


            URL remoteURL = new URL(urlString);

            HttpURLConnection httpServer = (HttpURLConnection) remoteURL.openConnection();
            httpServer.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            httpServer.setRequestProperty("Content-Language", "en-US");
            httpServer.setUseCaches(false);
            httpServer.setDoOutput(true);

            BufferedReader proxyToServerBR = new BufferedReader(new InputStreamReader(httpServer.getInputStream()));


            String line = "HTTP/1.0 200 OK\n" + "Proxy-agent: ProxyServer/1.0\n" + "\r\n";
            bw.write(line);


            while ((line = proxyToServerBR.readLine()) != null) {

                bw.write(line);

            }


            bw.flush();


            if (proxyToServerBR != null) {
                proxyToServerBR.close();
            }


            if (bw != null) {
                bw.close();
            }
        } catch (Exception e) {

        }
    }

}
