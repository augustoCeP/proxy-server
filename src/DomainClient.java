import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DomainClient extends Thread{
    private InputStream is;
    private OutputStream os;

    public DomainClient(InputStream is, OutputStream os){
        this.is = is;
        this.os = os;
    }

    @Override
    public void run() {


        try {

            // Read byte by byte from client and send directly to server
            byte[] buffer = new byte[4096];
            int read;
            do {
                read = is.read(buffer);
                if (read > 0) {
                    os.write(buffer, 0, read);
                    if (is.available() < 1) {
                        os.flush();
                    }
                }
            } while (read >= 0);
        }
        catch (IOException e) {
            System.out.println("Proxy to client HTTPS read timed out");
            e.printStackTrace();
        }
    }
}
