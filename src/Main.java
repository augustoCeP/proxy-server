import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {


    public static void main(String[] args){
        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(8080);

            while(true){
             Socket socket = serverSocket.accept();
             new ClientThread(socket).start();
            }

        }catch (IOException e){

        }
    }
}
