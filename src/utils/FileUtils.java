package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileUtils {
    private static File file;

    private static BufferedReader reader;

    public static boolean isBlockedDomain(String address) throws IOException {
        file = new File("BlockedDomains.txt");

        if (!file.exists()){
            file.createNewFile();
        }
        reader = new BufferedReader( new FileReader(file));

        String domain ="";
        while ((domain = reader.readLine()) != null){


            if (domain.equals(address)){
                return true;
            }
        }
        return false;
    }

    public static boolean isBlockedWord(String query) throws IOException{
        file = new File("BlockedWords.txt");
        reader = new BufferedReader( new FileReader(file));

        String domain ="";
        while ((domain = reader.readLine()) != null){
            if (domain.equals(query)){
                return true;
            }
        }
        return false;
    }



}
